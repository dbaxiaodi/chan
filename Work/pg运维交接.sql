
--by Chan

--每日备份文件
  D1、D3每天凌晨产生一次物理备份，
  脚本在宿主机/etc/crontab：
  00 06 * * * root /usr/bin/docker exec postgres-boss su - postgres -c 'sh /var/data/backup/data/gbdata_recovery/recovery.sh'
  00 06 * * * root /usr/bin/docker exec postgres-sites01 su - postgres -c 'sh /var/data/backup/data/gbdata_recovery/recovery.sh'

--主从同步
  D1、D3从库都在D2，但是D1 xlog在D2，D3 xlog在D3。
  流复制延迟情况，在从库上执行:
  SELECT extract(epoch from now() - pg_last_xact_replay_timestamp()) AS slave_lag;
  查询是否主从同步，可在主库上执行：
  select * from pg_stat_replication;

--D2主从同步配置
在容器内执行pg_basebackup同步postgres目录，再配置recovery.conf即可实时同步。
sites库命令参考如下：
  pg_basebackup -h 192.168.3.11 -U replicator  -D /var/data/postgres
  密码是：vd5e9431-3817-460x-a871-659d2e01ca99
vim recovery.conf
  standby_mode = on
  primary_conninfo = 'host=192.168.3.11 port=5432 user=replicator password=vd5e9431-3817-460x-a871-659d2e01ca99 application_name=postgres-sites01.d2'
  restore_command = 'cp /var/data/xlog_archive/%f %p'
以上是sites库配置，boss库自行参考。

--异地机房同步
  因为异地机房备份都是通过xlog同步。所以D1、D3将xlog分别放到D2、D3，然后分别同步到data-bak服务器（47.89.9.255），放在宿主机crontab：
  D2脚本如下/etc/crontab：
  * * * * * root flock -xn /var/run/rsync.lock -c 'rsync -avz --delete-before /var/gb/data/pg/pg_sites01/xlog_archive/* 47.89.9.255::postgres-sites01-bk'
  D3脚本如下/etc/crontab：
  * * * * * root flock -xn /var/run/rsync.lock -c 'rsync -avz --delete-before /var/gb/data/pg/pg_boss/xlog_archive/* 47.89.9.255::postgres-boss-bk'

--vacuum定时清理脏数据
  注意，如果多个vacuum进程同时在跑会影响服务器性能。ps查vacuum进程如果如果会冲突，要想办法重新调整定时。
  15,25,35 * * * * root /usr/bin/docker exec postgres-boss su - postgres -c "vacuumdb -h 127.0.0.1 --analyze -t api_order -U gb-companies gb-companies"
  00 03 * * * root /usr/bin/docker exec postgres-boss su - postgres -c "vacuumdb -h 127.0.0.1 --analyze --all"
  45 * * * * root sh /var/gb/script/pg_scripts/clean_dead_tuple.sh

--日志表定时清理
如下几张日志表要定时清理，目前还没做。
boss库：
task_run_record   10天左右
pay_log           10天左右
delete from game_api_log  where response_time < '2017-01-14 10:46:55';        10天
delete from game_api_transaction where create_time < '2016-12-24 14:52:15';   1个月
delete from mq_exception_msg where last_time < '2017-01-14 04:00:12';         10天
sites库:
delete from sys_audit_log where operate_time < '2016-12-24 12:49:40';         1个月
delete from player_game_log where login_time < '2016-12-24 07:43:37';         1个月

--生产脚本Flyway
  目前放在D2，但是要进入boss容器才可执行脚本，宿主机执行无法连接到容器ip
  进到boss容器之后，位置在：/var/data/backup/flyway-3.2.1
  site批量脚本位置在：/var/data/backup/script

--site分布情况（一个39个site）
-----D1-------+
 gb-site-112 |
 gb-site-113 |
 gb-site-114 |
 gb-site-115 |
 gb-site-119 |
 gb-site-125 |
 gb-site-131 |
 gb-site-139 |
 gb-site-71  |
 gb-site-76  |
 gb-site-143 |
(10 rows)

------D3-------+
    gb-site-1   |
    gb-site-110 |
    gb-site-111 |
    gb-site-116 |
    gb-site-117 |
    gb-site-118 |
    gb-site-120 |
    gb-site-121 |
    gb-site-122 |
    gb-site-123 |
    gb-site-124 |
    gb-site-126 |
    gb-site-127 |
    gb-site-128 |
    gb-site-129 |
    gb-site-130 |
    gb-site-132 |
    gb-site-133 |
    gb-site-134 |
    gb-site-135 |
    gb-site-136 |
    gb-site-137 |
    gb-site-138 |
    gb-site-140 |
    gb-site-141 |
    gb-site-142 |
    gb-site-73  |
    gb-site-74  |
    gb-site-75  |
   (29 rows)

--sites password
create user "postgres" superuser encrypted password 'Leisure+Chan=DBA2018nian';
create user "gb-boss" superuser encrypted password 'da76!aa#3cOb4@e340e044e1f54fcd5f';
create user "gb-companies" superuser encrypted password '99bO#3a@d5e!c59008093d3393b1ec6a';
create user "gb-stat" superuser encrypted password '88#3!cdac13d8d91781fb74@df8e4a4c';
create user "gb-site-1" superuser encrypted password 'bf#@982!4ebOb550f3f534b0baadf002';
create user "gb-site-71" superuser encrypted password '965#@a2dO7a!851d74a938059678c469';
create user "gb-site-75" superuser encrypted password 'wFAG7QR8YS@FT!kBmi6V#$6UnU4#&rxP';
create user "gb-site-73" superuser encrypted password '3h)>97]7(/8,(7m6|7h;$r6KY6J:HI,p';
create user "gb-site-74" superuser encrypted password 'G7~Dg~=^X@-mR1d@06V=u)WCxH8|(>z9';
create user "gb-site-76" superuser encrypted password ']d37rd1ZdD3041cf7ls6r2d^6h7.h763';   yongli8886.com  yongli8889.com  yl77.com
create user "gb-site-110" superuser encrypted password '5KAkl@nfjCYy6M&ZzEhWd40AJcRn3@E0';
create user "gb-site-111" superuser encrypted password 'V&OdsXbqPJRCtZIWNyq9UhfsS*P#Iz0#';
create user "gb-site-112" superuser encrypted password 'owhJ$nnf7K3y$q6^FkfL#s360AFVObar';  093193.com 092192.com
create user "gb-site-113" superuser encrypted password 'DVbofmfUz@qhkqHvyeU87!gmFgAqY9of';  weide7.com weide4.com
create user "gb-site-114" superuser encrypted password 'Ywvwxf0bHp37gyPRI277zVI7Bdz@SlcD';  7736123.com  77360033.com
create user "gb-site-115" superuser encrypted password 'u#KdQ93&vcUtTbaGgU~OyJrZVV30SP3c';  lol919.com  yingxionglianmeng8.com
create user "gb-site-116" superuser encrypted password '8FMwiW5X#9CT5qYyAcCW3cfQFL2F5^zc';  qiqiqi4.com qiqiqi8.com
create user "gb-site-117" superuser encrypted password 'oYP&WU#E@CpSrEPL4SBhb2daS0fE9$22';  73382222.com  73381111.com
create user "gb-site-118" superuser encrypted password '&H3@3Kqk0DuaMG1zTS$PsYb@g4xaQRaI';  7882800.com 6223003.com
create user "gb-site-119" superuser encrypted password 'K1P*2j^xpTp&HZVZYSIlDosI%5G2bzz!';  ued69.com ued38.com
create user "gb-site-120" superuser encrypted password '*3xZ12G9XUR&z3p@cWBpBqmjc14@jr3m';
create user "gb-site-121" superuser encrypted password 'lGcysxcTA$DeGhtZeJn*9hmrCNK^APqc';  8138bet.com 138460.com
create user "gb-site-122" superuser encrypted password 'nsOhfC7VL^h4ADZsnjU1$Ue4lJQSPuJR';
create user "gb-site-123" superuser encrypted password 'Xl^%$Ssp7f!j^vm%7f1f45uzBGCbbAf5';  d91019.com  801478.com
create user "gb-site-124" superuser encrypted password 'UWlEUfDa7Qi39y483k*%K6Fy7XeOnfjA';  850588888.com 850587777.com
create user "gb-site-125" superuser encrypted password '#g6b8xrX%ajU26ilA&%psYrlHd@tE86D';  999258d.com 999258a.com
create user "gb-site-126" superuser encrypted password '$$xYuR1cb5ttD^rCYXvkW1b%K@0ivkI@';  yy463344.com  zz463344.com
create user "gb-site-127" superuser encrypted password '^*vhFVX39e^vRtKa0SOCmXL!0IAT9^O#';
create user "gb-site-128" superuser encrypted password 'MZHkh33xZ$$t4K4qzHebQHMhmI&Mz^oY';  xpj88770.com  xpj88110.com
create user "gb-site-129" superuser encrypted password 'cOrl7*zFrYvo*dFwW$HV46NrA&O06p7O';  p66608.com  p66607.com
create user "gb-site-130" superuser encrypted password '6Uebg#TfcVu^95ZNt2z!ZW36S35^$yXu';
create user "gb-site-131" superuser encrypted password 'hL!ShnBDUJTS$n44JSbM31i2amp!7rGx';  8750018.com 8750016.com
create user "gb-site-132" superuser encrypted password '63jO#YUcYmSukpy008En9mOp%&cILaN@';
create user "gb-site-133" superuser encrypted password '%L##6Ja90Sgw4chYuzb3bXNFiWLDLfV1';  vns9207.com vns9103.com
create user "gb-site-134" superuser encrypted password '^L!vT7Y7eagh7XI7LXbN2ZzEhoNpaQ8U';  119441.com  224770.com
create user "gb-site-135" superuser encrypted password 'Kyf1lawHr#j6HnES&9Zw3vc8khD5FWgD';  946502.com  946504.com
create user "gb-site-136" superuser encrypted password 'Y$6HC9NPuf4qmOifmM#k^ofGi#pHUg4*';  wns90.com wns80.com
create user "gb-site-137" superuser encrypted password 'k^Zi5CK9GXnpA6hz@4G9vqGGCbR5sSx$';  k9868.com bc1883.com
create user "gb-site-138" superuser encrypted password 'gS1JdkyOhCjR^HUEb&#!Vrnz$4rqd7o^';
create user "gb-site-139" superuser encrypted password 'pbOTAa1LG35V0KzKr&2m8cEjiHnU60Ip';  7910yy.com  7910a.com
create user "gb-site-140" superuser encrypted password 'fJ$x8zPE1gmTqN5Lp!C70#O$Zu*xgqPF';  1amvns.com  5080855.com
create user "gb-site-141" superuser encrypted password 'OdT@CUE3qPv&wsXZ@kiV49o#S!m%L#i^';  bet3659c.com  bet3659a.com
create user "gb-site-142" superuser encrypted password 'i#&4eW%GA^gz8TqZ!ljW9rsmAH8NE4s5';  29985555.com  29986666.com
create user "gb-site-143" superuser encrypted password '4S92S0CB76gQCHhyLCDBWAeuXJqkn5@#';  729330.com   751330.com
create user "gb-site-144" superuser encrypted password 'P247Js01bwX99lp*%!@dOqB195wW!Zlt';
create user "gb-site-145" superuser encrypted password '#mjSi!J7ZVKKMftu4mh@Kr$Xi1WD37vn';
create user "gb-site-146" superuser encrypted password 'g#f^3IdcSISlOiUFrI2bFel9e0!kgy1S';
create user "gb-site-147" superuser encrypted password 'yQ8UPWl1y0RKB1S%B^Bwszy$FhaQ5aB3';
create user "gb-site-148" superuser encrypted password 'bhsWAHBHC8$r5LW5uV!!syVG8Lpdzkm9';
create user "gb-site-149" superuser encrypted password '^woJknHDJCQBGq&upX0p8O!hBDKO95u4';
create user "gb-site-150" superuser encrypted password 'P247Js01bwX99lp*%!@dOqB195wW!Zlt';  07918899a.com 07918899.com  07918899b.com
create user "gb-site-151" superuser encrypted password '#mjSi!J7ZVKKMftu4mh@Kr$Xi1WD37vn';  501365e.com 501365u.com
create user "gb-site-153" superuser encrypted password 'yQ8UPWl1y0RKB1S%B^Bwszy$FhaQ5aB3';  a77627.com  b77627.com
create user "gb-site-155" superuser encrypted password 'bhsWAHBHC8$r5LW5uV!!syVG8Lpdzkm9';
create user "gb-site-156" superuser encrypted password '^woJknHDJCQBGq&upX0p8O!hBDKO95u4';
create user "gb-site-157" superuser encrypted password 'g#f^3IdcSISlOiUFrI2bFel9e0!kgy1S';
--create schema
create schema "gb-site-142" authorization "gb-site-142";

==================
1 	119
2 	135
3 	112
4 	76
5 	124
6 	121
7 	113
8 	123
9 	114
10 	126
11 	128
12 	125
13 	111
14 	129
15 	133
16 	118
17 	139
18 	115
19 	137
20 	134
21 	140
