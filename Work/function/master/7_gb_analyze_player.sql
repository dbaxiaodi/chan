drop function if exists gb_analyze_player(DATE, TIMESTAMP, TIMESTAMP);
create or replace function gb_analyze_player(
	stat_date 	DATE,
	start_time 	TIMESTAMP,
	end_time 	TIMESTAMP
) returns void as $$
/*版本更新说明
  版本   时间        作者     内容
--v1.00  2015/12/10  Leisure  创建此函数: 经营分析-玩家
--v1.01  2015/12/19  Leisure  取款改为实际取款。存取款改由存取款表获取
*/
DECLARE
	rec 	record;
	gs_id 	INT;
	n_count 	INT;

BEGIN

	raise info '清除 % 号统计数据...', stat_date;
	DELETE FROM analyze_player WHERE static_date = stat_date;
	GET DIAGNOSTICS n_count = ROW_COUNT;
	raise notice '本次删除记录数 %', n_count;

	raise info '统计 % 号经营数据.START', stat_date;

	WITH up AS
	(
	  SELECT su."id" player_id,
	         ua."id" agent_id,
	         ut."id" topagent_id,
	         su.register_site,
	         su.create_time >= start_time AND su.create_time < end_time is_new_player
	    FROM sys_user su
	    LEFT JOIN  sys_user ua ON ua.user_type= '23' AND su.owner_id = ua."id"
	    LEFT JOIN  sys_user ut ON ut.user_type= '22' AND ua.owner_id = ut."id"
	   WHERE su.user_type = '24'
	),

	pr AS (
	        --存款
	         SELECT player_id,
	                --'deposit' AS transaction_type,
	                SUM(recharge_amount) deposit_amount
	           FROM player_recharge
	          WHERE recharge_status IN ('2', '5')
	            AND create_time >= start_time AND create_time < end_time
	          GROUP BY player_id
	),

	pw AS (
	         --取款
	         SELECT player_id,
	               --'withdrawal' AS transaction_type,
	               SUM(withdraw_actual_amount) withdraw_amount
	           FROM player_withdraw
	          WHERE withdraw_type IN ('manual_deposit', 'first', 'normal')
	            AND withdraw_status = '3'
	            AND create_time >= start_time AND create_time < end_time
	          GROUP BY player_id
	),

	pgo AS (
	  SELECT player_id,
	         SUM(effective_trade_amount) effective_amount,
		       SUM(profit_amount) payout_amount
	    FROM player_game_order
	   WHERE order_state = 'settle'
	     AND is_profit_loss = TRUE
	     AND payout_time >= start_time
	     AND payout_time < end_time
	   GROUP BY player_id
	)

	INSERT INTO analyze_player (
	    player_id,
	    agent_id,
	    topagent_id,
	    promote_link,
	    is_new_player,
	    deposit_amount,
	    withdraw_amount,
	    effective_amount,
	    payout_amount,
	    static_date,
	    static_time,
	    static_time_end
	)
	SELECT up.*, pr.deposit_amount, pw.withdraw_amount, pgo.effective_amount, pgo.payout_amount, start_time, end_time, stat_date
	  FROM up
	  LEFT JOIN pr ON up.player_id = pr.player_id
	  LEFT JOIN pw ON up.player_id = pw.player_id
	  LEFT JOIN pgo ON up.player_id = pgo.player_id;

	GET DIAGNOSTICS n_count = ROW_COUNT;
	raise notice 'analyze_player新增记录数 %', n_count;

	raise info '统计 % 号经营数据.END', stat_date;

END;

$$ language plpgsql;
COMMENT ON FUNCTION gb_analyze_player(stat_date DATE, start_time TIMESTAMP, end_time TIMESTAMP)
IS 'Leisure-经营分析-玩家';
