
--查看占用空间大且使用次数低于100的索引
SELECT schemaname, relname, indexrelname, idx_scan,  pg_catalog.pg_size_pretty(pg_catalog.pg_table_size(indexrelid)) as "Size"  
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pk%'
   AND schemaname = 'gb-site-9' 
   AND idx_scan <= 10 
 ORDER BY pg_catalog.pg_table_size(indexrelid) DESC 
 LIMIT 30;

--查看所有site库的索引情况
SELECT relname, indexrelname, SUM(idx_scan) idx_scan,  pg_catalog.pg_size_pretty(SUM(pg_catalog.pg_table_size(indexrelid))) as "Size"--, ARRAY_AGG(schemaname)
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pk%' 
 GROUP BY relname, indexrelname
HAVING SUM(idx_scan) <= 0
 ORDER BY SUM(pg_catalog.pg_table_size(indexrelid)) DESC
 LIMIT 30;

--生成drop语句
SELECT DISTINCT 'DROP INDEX ' || indexrelname || ';' FROM (
SELECT schemaname, relname, indexrelname, idx_scan,  pg_catalog.pg_size_pretty(pg_catalog.pg_table_size(indexrelid)) as "Size"  
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pk%'
   AND schemaname = 'gb-site-9' 
   AND idx_scan <= 10 
 ORDER BY pg_catalog.pg_table_size(indexrelid) DESC 
) idx
ORDER BY 1;

--禁用索引
UPDATE pg_index SET indisvalid = false WHERE indexrelid = 'api_order_part_idx'::regclass;

--删除索引，如果遇到锁的处理
SELECT 'SELECT pg_terminate_backend('''||pid||''');' FROM pg_stat_activity WHERE datname='gb-companies'
UNION ALL
SELECT 'DROP INDEX api_order_idx2;';

SELECT DISTINCT 'DROP INDEX ' || indexrelname || ';' FROM (
SELECT indexrelname
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pk%'
   AND schemaname = 'gb-site-30' 
EXCEPT
SELECT indexrelname
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pk%'
   AND schemaname = 'gb-site-9'
) idx
ORDER BY 1;
