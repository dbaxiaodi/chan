Leisure.Yo（李逍遥）  12:06
gb-sites=# EXPLAIN ANALYZE
select COALESCE(sum(profit_amount),0) sumProfitAmount 
  from player_game_order 
 where bet_date between '2016-07-20' 
   and '2016-08-19' and is_profit_loss = true 
 GROUP BY bet_date
 --ORDER BY sumProfitAmount desc 
 LIMIT 1;
                                                                          QUERY PLAN                                                                           
---------------------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=0.43..8.47 rows=1 width=9) (actual time=0.082..0.082 rows=1 loops=1)
   ->  GroupAggregate  (cost=0.43..8.47 rows=1 width=9) (actual time=0.080..0.080 rows=1 loops=1)
         Group Key: bet_date
         ->  Index Scan using idx_player_game_order_bet_date on player_game_order  (cost=0.43..8.45 rows=1 width=9) (actual time=0.037..0.059 rows=23 loops=1)
               Index Cond: ((bet_date >= '2016-07-20'::date) AND (bet_date <= '2016-08-19'::date))
               Filter: is_profit_loss
 Planning time: 0.171 ms
 Execution time: 0.123 ms
(8 rows)

Time: 0.637 ms















































