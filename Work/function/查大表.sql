﻿--查大表
SELECT n.nspname as "Schema",
  c.relname as "Name",
  CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'm' THEN 'materialized view' WHEN 'i' THEN 'index' WHEN 'S' THEN 'sequence' WHEN 's' THEN 'special' WHEN 'f' THEN 'foreign table' END as "Type",
  pg_catalog.pg_get_userbyid(c.relowner) as "Owner",
  pg_catalog.pg_size_pretty(pg_catalog.pg_table_size(c.oid)) as "Size",
  pg_catalog.obj_description(c.oid, 'pg_class') as "Description"
FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('r','v','m','S','f','')
      AND n.nspname <> 'pg_catalog'
      AND n.nspname <> 'information_schema'
      AND n.nspname !~ '^pg_toast'
      AND pg_catalog.pg_table_is_visible(c.oid) --注释此行用超级用户可以查看所有表
ORDER BY pg_catalog.pg_table_size(c.oid) DESC, 1, 2
LIMIT 30;

--查事务
SELECT * FROM pg_stat_activity WHERE usename = 'gb-site-9' AND state <> 'idle';

--VACUUM FULL，如果遇到锁的处理
SELECT 'SELECT pg_terminate_backend('''||pid||''');' FROM pg_stat_activity WHERE usename = 'gb-site-9' AND query LIKE '%player_game_order%' AND state = 'active' AND application_name <> 'psql'
 UNION ALL
SELECT 'VACUUM FULL player_game_order;';
