\x on

--单次最慢语句
SELECT pg_get_userbyid(userid) AS user, query, calls, total_time, (total_time/calls) as average, rows, 
        100.0 * shared_blks_hit /nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent 
  FROM pg_stat_statements 
 WHERE calls > 9
   AND upper(query) NOT LIKE 'UPDATE%'
   AND upper(query) NOT LIKE 'INSERT%'
 ORDER BY average DESC LIMIT 10;

--总用时最久的语句
SELECT pg_get_userbyid(userid) AS user, query, calls, total_time, (total_time/calls) as average, rows, 
        100.0 * shared_blks_hit /nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent 
  FROM pg_stat_statements 
 WHERE calls > 9
   AND upper(query) NOT LIKE 'UPDATE%'
   AND upper(query) NOT LIKE 'INSERT%'
 ORDER BY total_time DESC
 LIMIT 10;

--总用时最久的100个语句中，最慢的10条
SELECT * FROM 
(
SELECT pg_get_userbyid(userid) AS user, query, calls, total_time, (total_time/calls) as average, rows, 
        100.0 * shared_blks_hit /nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent 
  FROM pg_stat_statements 
 WHERE calls > 9
   AND upper(query) NOT LIKE 'UPDATE%'
   AND upper(query) NOT LIKE 'INSERT%'
 ORDER BY total_time DESC LIMIT 100
) avg
ORDER BY average DESC
LIMIT 10;
