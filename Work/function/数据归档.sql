﻿--注单数据保持40天，所以每月11号归档上个月即可

--查看最新的备份表
\d player_game_order_bak*

\timing on
SELECT COUNT(*) FROM player_game_order WHERE payout_time < '2016-10-01' AND order_state = 'settle';

--保证备份和删除在一个事务里面
BEGIN;

CREATE TABLE player_game_order_bak_20161001 AS SELECT * FROM player_game_order WHERE payout_time < '2016-10-01' AND order_state = 'settle';

--DELETE后面的部分，从上面的语句复制，保证两次条件一致
DELETE FROM player_game_order WHERE payout_time < '2016-10-01' AND order_state = 'settle';

--确认删除条数，和查询的结果是一样的才可以END;，否则ROLLBACK;
END;

--删除之后清理一下垃圾
VACUUM ANALYZE player_game_order;
