
--查看占用空间大且使用次数低于100的索引
SELECT schemaname, relname, indexrelname, idx_scan,  pg_catalog.pg_size_pretty(pg_catalog.pg_table_size(indexrelid)) as "Size"  
  FROM pg_stat_user_indexes 
 WHERE indexrelname NOT LIKE '%pkey' 
   AND idx_scan < 100 
 ORDER BY pg_catalog.pg_table_size(indexrelid) DESC;

--禁用索引
UPDATE pg_index SET indisvalid = false WHERE indexrelid = 'api_order_part_idx'::regclass;

--删除索引，如果遇到锁的处理
SELECT 'SELECT pg_terminate_backend('''||pid||''');' FROM pg_stat_activity WHERE datname='gb-companies'
UNION ALL
SELECT 'DROP INDEX api_order_idx2;';
