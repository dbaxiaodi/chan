


1.companies库中api_order脏数据清理。
/usr/bin/docker exec postgres-gb.d2 su - postgres -c "psql -U gb-companies -c 'vacuum analyze verbose api_order;'"
=======================
2.导出某张表。
pg_dump -U gb-companies -t system_announcement -d gb-companies > ./system_announcement_04152155.sql
=======================
3.导出某个schema：
pg_dump -U "gb-site-3" -n "gb-site-3" gb-sites > /pgdata/export/site-3_01311657.sql
============================
4.dump记录为insert语句：
select * into test from task_schedule where job_name like '%线上支付超时%';
pg_dump -U gb-boss -t test --inserts > /var/data/backup/update/task_schedule_11132204.sql
===========================================
5.streaming replication delay流复制延迟:
# SELECT extract(epoch from now() - pg_last_xact_replay_timestamp()) AS slave_lag;
slave_lag
-----------
33.50896
===============================
6.表导出excel文件并转码gbk：
copy (select * from sys_domain) to '/var/data/backup/update/sys_domain_11201035.csv' delimiter ',' csv header;
iconv -f utf-8 -t gbk sys_domain_11201035.csv -o sys_domain_11201046.csv
=======================================
7.扩展pg_stat_statements 安装与使用
gb-stat=# create extension pg_stat_statements;
	查询方法：
SELECT  query, calls, total_time, (total_time/calls) as average ,rows,
        100.0 * shared_blks_hit /nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent
FROM    pg_stat_statements
ORDER   BY average DESC LIMIT 10;
========pg help==========================
gb-sites=# select schemaname,relname, n_live_tup,n_dead_tup,now() from pg_stat_all_tables where relname='player_game_order' order by n_dead_tup desc limit 10;
gb-companies=# select schemaname, n_live_tup,n_dead_tup,now() from pg_stat_all_tables where relname='api_order';
gb-sites=# select usename, count(*) from pg_stat_activity group by usename order by count desc;
gb-sites=# select count(*) from pg_stat_activity where datname = 'gb-sites' and usename = 'gb-site-9' and state = 'active';
[postgres@postgres-boss update]$ pg_dump -h127.0.0.1 -U gb-boss -t test --data-only --column-inserts gb-boss > /var/data/backup/update/001.sql
reindexdb --verbose --all
gb-companies=# SELECT extract(epoch from now() - pg_last_xact_replay_timestamp()) AS slave_lag;
gb-companies=# select 'SELECT pg_terminate_backend('''||pid||''');' from pg_stat_activity where wait_event_type = 'Lock';
