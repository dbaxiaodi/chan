

    遇到脏数据表特别大的时候，直接vacuum 会让服务器卡死，简单的方法是表重建，秒级别完成。

1.生产导出表结构：pg_dump -h127.0.0.1 -U gb-companies -t test --schema-only  gb-companies > /var/data/backup/update/test.sql
2.重命名后建表：alter table test rename TO test_old;   CREATE TABLE test...
3.执行插入：insert into(a, b, c) test select a, b, c from test_old ON conflict (api_id, bi_id) do nothing;

步骤二：建表完成后执行插入语句
INSERT INTO test (a, b, c) select a, b, c from test_old ON conflict (api_id, bi_id) do nothing;

步骤一：建表并重命名索引（注意要在一个事务上完成）
begin;
--重命名旧表，索引以及序列
alter table test rename TO test_old;
alter sequence test_id_seq rename to test_id_seq_old;
alter index test_pkey rename to test_pkey_old;
alter index u_test rename to u_test_old;
alter index test_part_idx4 rename to test_part_idx4_old;
alter index fk_test_site_id rename to fk_test_site_id_old;
alter index idx_test_composite_1 rename to idx_test_composite_1_old;

--建表，具体表结构自行到生产dump，以下只给出样本
CREATE TABLE test (
    id integer NOT NULL,
    game_code character varying(64),
    account character varying(32) NOT NULL,
    single_amount numeric(20,2),
    payout_time timestamp(6) without time zone,
    is_profit_loss boolean,
    currency_code character varying(30),
    result_json text,
    action_id_json text,
);

end;
