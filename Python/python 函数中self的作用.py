
1.self只有在class中才有，单独函数中不需要
2.self是对象本身，通过代码可以看
3.self关键字可以用其它字符替换，但还是遵守约定比较好。

简单例子：
class Python():
    def test(self):
    print(self)
    print(__class__)

p = Python()
p.test()

----------------
<__main__.Python object at 0x00000000006AA3C8>
<class '__main__.Python'>
