
    经常在py脚本中看到这样的代码(if __name__ == '__main__':)，作用是让代码可以自己执行也可被import！
这样既可以让“模块”文件运行，也可以被其他模块引入，而且不会执行函数两次。具体看代码理解：

创建第一个脚本：
#module.py
def first():
    print('we are in %s' % __name__)

if __name__ == '__main__':
    print(first())
----------------------------------------
#输出如下，可以看出__name__ == '__main__'：
we are in __main__

创建第二个脚本，import第一个脚本函数：
#anothermodule.py
from module import first
print(first())
-------------------
#输出name得到的是自身的文件名，而不再是'__main__'
we are in module

总结一下：
    如果直接执行module.py文件，那么__name__ == '__main__'是True，
但如果是被import导入，那它就是py的文件名而不再是__main__
